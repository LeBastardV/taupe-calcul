import React, { useState } from "react";
import { CssBaseline, makeStyles } from "@material-ui/core";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NavBar from './NavBar';
import Home from './Home';
import Addition from './Addition';
import Soustraction from './Soustraction';
import Multiplication from './Multiplication';
import Division from './Division';
import Classement from './Classement';
import SignUp from './SignUp';
import SignIn from './SignIn';
import { AuthProvider } from './Authenticate';

const drawerWidth = 240;

const useStyles = makeStyles(() => ({
  content: {
    marginLeft: drawerWidth,
  },
}));

export default function App() {
  const classes = useStyles();
  const [email, setEmail] = useState('');

  return (
    <div className="App">
      <AuthProvider>
        <Router>
          <CssBaseline />
          <NavBar />
          <div className={classes.content}>
            <Switch >
              <Route exact from="/" component={Home} />
              <Route exact from="/signin" render={props => <SignIn setEmail={setEmail} email={email} />} />
              <Route exact from="/signup" component={SignUp} />
              <Route exact from="/addition" component={Addition} />
              <Route exact from="/soustraction" component={Soustraction} />
              <Route exact from="/multiplication" component={Multiplication} />
              <Route exact from="/division" component={Division} />
              <Route exact from="/classement" component={Classement} />
            </Switch>
            <p>{email}</p>
          </div>
        </Router>
      </AuthProvider>
    </div>
  );
}