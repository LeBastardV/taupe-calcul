import { Typography } from '@material-ui/core';
import React from 'react';
import fire from './fire';


export default function Addition() {

    const getScore = () => {
        var users;
        const scores =
            fire.database()
                .ref('users')
                .orderByValue()
                .once('value');

        return scores
    }

    return (
        <div>
            <Typography variant="h3">
                Classement {getScore()}
            </Typography>
        </div>
    );
}