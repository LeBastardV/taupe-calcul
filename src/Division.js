import { FormControl, FormLabel, RadioGroup, FormControlLabel, Radio, Button, makeStyles, Typography, Card, CardActions } from '@material-ui/core';
import React, { useState } from 'react';
import Taupe from './taupe.png'
import fire from './fire';

const useStyles = makeStyles(() => ({
    form: {
        textAlign: "center",
    },
    formControl: {
        margin: "1rem",
    },
    button: {
        margin: "1rem",
    },
    img: {
        maxWidth: "75px",
    },
    card: {
        display: 'inline-block',
        margin: '5vw'
    },
    resultCalcul: {
        display: 'inline-block'
    }
}));

export default function Division() {
    const classes = useStyles();
    const [table, setTable] = useState();
    const [tableChoosed, setTableChoosed] = useState(false);
    const [randomInt, setRandomInt] = useState();
    const [answer, setAnswer] = useState();
    const [answerArray, setAnswerArray] = useState();
    const [resultCalcul, setresultCalcul] = useState('');
    const [colorResult, setColorResult] = useState('')

    const handleRadioChange = (event) => {
        setTable(parseInt(event.target.value));
    };

    const handleRandom = () => {
        let value = parseInt(Math.floor(Math.random() * 12));
        setRandomInt(value);
        setAnswer(table / value);
        arrayAnswer(table / value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        setTableChoosed(true);
        handleRandom();
    };

    const getRandom = () => {
        return parseInt(Math.floor(Math.random() * 40));
    }

    const arrayAnswer = (answer) => {
        let array = [answer, getRandom(), getRandom(), getRandom()]
        console.log("test", answer)
        var currentIndex = array.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        setAnswerArray(array);
    }

    const getUserId = () => {
        var userId = fire.auth().currentUser.uid;
        return userId;
    }

    const getScore = () => {
        var scoreValue
        var userId = getUserId();
        fire
            .database()
            .ref("users/" + userId)
            .on("value", snapshot => {
                const data = snapshot.val();
                scoreValue = (data.score);
            })
        return scoreValue
    }

    const updateScore = (value) => {
        var userId = getUserId();
        fire
            .database()
            .ref('users/' + userId)
            .update({ score: value });
    }

    const clickButton = (event, value) => {
        event.preventDefault();
        setresultCalcul(value);
        if (value === answer) {
            setresultCalcul("Felicitation tu as trouvé la bonne réponse (+1 point)");
            setColorResult('green');
            if (fire.auth().currentUser != null) {
                var score = getScore();
                updateScore(score + 1)
            }

        } else {
            setresultCalcul("Mauvaise réponse, tu ne gagnes pas de point");
            setColorResult('red');
        }
        setTableChoosed(false)
    }

    if (!tableChoosed) {
        return (
            <div className={classes.form}>
                <Typography variant="h3">
                    Division
                </Typography>
                <Typography className={classes.resultCalcul} style={{ color: colorResult }}>{resultCalcul}</Typography>

                <form onSubmit={handleSubmit} >
                    <FormControl component="fieldset" className={classes.formControl}>
                        <FormLabel component="legend">Choisis une table :</FormLabel>
                        <RadioGroup aria-label="Table" name="Table" onChange={handleRadioChange}>
                            <FormControlLabel value="1" control={<Radio />} label="1" />
                            <FormControlLabel value="2" control={<Radio />} label="2" />
                            <FormControlLabel value="3" control={<Radio />} label="3" />
                            <FormControlLabel value="4" control={<Radio />} label="4" />
                            <FormControlLabel value="5" control={<Radio />} label="5" />
                            <FormControlLabel value="6" control={<Radio />} label="6" />
                            <FormControlLabel value="7" control={<Radio />} label="7" />
                            <FormControlLabel value="8" control={<Radio />} label="8" />
                            <FormControlLabel value="9" control={<Radio />} label="9" />
                            <FormControlLabel value="10" control={<Radio />} label="10" />
                        </RadioGroup>
                        <Button type="submit" variant="outlined" color="primary" className={classes.button}>
                            Choisir
                    </Button>
                    </FormControl>
                </form>

            </div>
        );
    } else {
        return (
            <div className={classes.form}>
                <Typography variant="h2">{table} ÷ {randomInt} = </Typography>
                <Typography></Typography>
                <Card className={classes.card}>
                    <CardActions>
                        <Button size="small" onClick={e => clickButton(e, answerArray[0])}>
                            <img src={Taupe} alt="Taupe" className={classes.img} />
                            <Typography variant="h4"> {answerArray[0]} </Typography>
                        </Button>
                    </CardActions>
                </Card>

                <Card className={classes.card}>
                    <CardActions>
                        <Button size="small" onClick={e => clickButton(e, answerArray[1])}>
                            <img src={Taupe} alt="Taupe" className={classes.img} />
                            <Typography variant="h4"> {answerArray[1]} </Typography>
                        </Button>
                    </CardActions>
                </Card>

                <Card className={classes.card}>
                    <CardActions>
                        <Button size="small" onClick={e => clickButton(e, answerArray[2])}>
                            <img src={Taupe} alt="Taupe" className={classes.img} />
                            <Typography variant="h4"> {answerArray[2]} </Typography>
                        </Button>
                    </CardActions>
                </Card>

                <Card className={classes.card}>
                    <CardActions>
                        <Button size="small" onClick={e => clickButton(e, answerArray[3])}>
                            <img src={Taupe} alt="Taupe" className={classes.img} />
                            <Typography variant="h4"> {answerArray[3]} </Typography>
                        </Button>
                    </CardActions>
                </Card>
            </div >
        );
    }
}