import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import Taupe from './taupe.png';
import fire from './fire';

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
    },
    banner: {
        textAlign: "center",
        height: "15vh",
        background: "#8f791788",
    },
    imgbanner: {
        maxHeight: "10vh",
    },
    textContent: {
        fontSize: "1.5rem",
        margin: "5vh 5vw",
    },
}));
export default function ButtonAppBar() {
    const classes = useStyles();

    const getUserId = () => {
        if (fire.auth().currentUser != null) {
            var userId = fire.auth().currentUser.uid;
            return userId;
        }
    }

    const getName = () => {
        var name;
        var userId = getUserId();
        fire
            .database()
            .ref("users/" + userId)
            .on("value", snapshot => {
                const data = snapshot.val();
                name = (data.firstname);
            })
        return name;
    }

    return (
        <div className={classes.root}>
            <Typography variant="h1" className={classes.banner}>
                <img src={Taupe} alt="Taupe" className={classes.imgbanner} />
                    Taupe calcul
                <img src={Taupe} alt="Taupe" className={classes.imgbanner} />
            </Typography>

            <div className={classes.textContent} >
                <p>Bienvenue {getName()}, </p>
                <p>Taupe calcul est un projet d'étude consistant à créer une application web.</p>
                <p>Cette dernière a pour but de permettre de réviser ses tables d'opérations
                    (addition, soustraction, multiplication, division).</p>
                <p>Il est possible de choisir le type d'opération que vous souhaitez reviser dans le menu de gauche.</p>
            </div>
        </div>
    );
}