import React, { useContext } from 'react';
import { AppBar, CssBaseline, makeStyles, Toolbar, Typography, Button, Drawer as MUIDrawer, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import HomeIcon from '@material-ui/icons/Home';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import ClearIcon from '@material-ui/icons/Clear';
import TableChartIcon from '@material-ui/icons/TableChart';
import VerticalAlignCenterIcon from '@material-ui/icons/VerticalAlignCenter';
import { withRouter } from "react-router-dom";
import fire from './fire';
import { AuthContext } from "./Authenticate";


const drawerWidth = 240;

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        background: "#8f791788",
    },
    drawerPaper: {
        width: drawerWidth,
        background: '#8f6732'
    },
}));


const NavBar = props => {
    const classes = useStyles();
    const { history } = props;

    const itemsList = [
        {
            text: "Home",
            icon: <HomeIcon />,
            onClick: () => {
                history.push("/");
            }
        },
        {
            text: "Addition",
            icon: <AddIcon />,
            onClick: () => {
                history.push("/addition");
            }
        },
        {
            text: "Soustraction",
            icon: <RemoveIcon />,
            onClick: () => {
                history.push("/soustraction");
            }
        },
        {
            text: "Multiplication",
            icon: <ClearIcon />,
            onClick: () => {
                history.push("/multiplication");
            }
        },
        {
            text: "Division",
            icon: <VerticalAlignCenterIcon />,
            onClick: () => {
                history.push("/division");
            }
        },
        {
            text: "Classement",
            icon: <TableChartIcon />,
            onClick: () => {
                history.push("/classement");
            }
        }
    ];

    const { currentUser } = useContext(AuthContext);

    function Connect() {
        if (currentUser) {
            return (<Button color="inherit" onClick={() => { fire.auth().signOut(); window.location.reload(); }}>Log Out</Button>);
        }
        else {
            return (
                <div>
                    <Button color="inherit" onClick={() => history.push("/signin")}>Sign In</Button>
                    <Button color="inherit" onClick={() => history.push("/signup")}>Sign Up</Button>
                </div>)
        }
    }


    return (
        <div className={classes.root} >
            <CssBaseline />
            <AppBar position="static" style={{ background: '#8f6732' }} className={classes.appBar}>
                <Toolbar>
                    <Typography variant="h6" style={{ flexGrow: 1 }}>
                        Taupe-Calcul
                    </Typography>
                    <Connect />
                </Toolbar>
            </AppBar>
            <MUIDrawer
                anchor='left'
                variant="permanent"
                className={classes.drawer}
                style={{ background: '#8f6732' }}
                classes={{
                    paper: classes.drawerPaper,
                }}>
                <div style={{ color: 'white' }}>
                    <List>
                        {itemsList.map((item) => {
                            const { text, icon, onClick } = item;
                            return (
                                <ListItem button key={text} onClick={onClick}>
                                    {icon && <ListItemIcon>{icon}</ListItemIcon>}
                                    <ListItemText primary={text} />
                                </ListItem>
                            );
                        })}
                    </List>
                </div>
            </MUIDrawer>
        </div >
    );
}
export default withRouter(NavBar);