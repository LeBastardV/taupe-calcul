import firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyBQ1tBzxeMD2cMknT7rZg_9M3fzwc2A4dM",
    authDomain: "taupe-calcul.firebaseapp.com",
    databaseURL: "https://taupe-calcul-default-rtdb.firebaseio.com",
    projectId: "taupe-calcul",
    storageBucket: "taupe-calcul.appspot.com",
    messagingSenderId: "316717069259",
    appId: "1:316717069259:web:27437e3cb717eda0f47c4e",
    measurementId: "G-9WS41KG30C"
};

var fire = firebase.initializeApp(firebaseConfig);

export default fire;